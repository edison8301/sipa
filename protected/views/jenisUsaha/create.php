<?php
$this->breadcrumbs=array(
	'Jenis Usahas'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List JenisUsaha','url'=>array('index')),
array('label'=>'Manage JenisUsaha','url'=>array('admin')),
);
?>

<h1>Create JenisUsaha</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>