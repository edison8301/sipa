<?php
$this->breadcrumbs=array(
	'Jenis Usahas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List JenisUsaha','url'=>array('index')),
	array('label'=>'Create JenisUsaha','url'=>array('create')),
	array('label'=>'View JenisUsaha','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage JenisUsaha','url'=>array('admin')),
	);
	?>

	<h1>Update JenisUsaha <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>