<?php
$this->breadcrumbs=array(
	'Jenis Usaha'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah Jenis Usaha','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('jenis-usaha-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Jenis Usaha</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'jenis-usaha-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
