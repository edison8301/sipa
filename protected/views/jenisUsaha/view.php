<?php
$this->breadcrumbs=array(
	'Jenis Usahas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List JenisUsaha','url'=>array('index')),
array('label'=>'Create JenisUsaha','url'=>array('create')),
array('label'=>'Update JenisUsaha','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete JenisUsaha','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage JenisUsaha','url'=>array('admin')),
);
?>

<h1>View JenisUsaha #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
