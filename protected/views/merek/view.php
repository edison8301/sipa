<?php
$this->breadcrumbs=array(
	'Mereks'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Merek','url'=>array('index')),
array('label'=>'Create Merek','url'=>array('create')),
array('label'=>'Update Merek','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Merek','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Merek','url'=>array('admin')),
);
?>

<h1>View Merek #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
