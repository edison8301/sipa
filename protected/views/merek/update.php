<?php
$this->breadcrumbs=array(
	'Mereks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Merek','url'=>array('index')),
	array('label'=>'Create Merek','url'=>array('create')),
	array('label'=>'View Merek','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Merek','url'=>array('admin')),
	);
	?>

	<h1>Update Merek <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>