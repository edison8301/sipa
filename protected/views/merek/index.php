<?php
$this->breadcrumbs=array(
	'Mereks',
);

$this->menu=array(
array('label'=>'Create Merek','url'=>array('create')),
array('label'=>'Manage Merek','url'=>array('admin')),
);
?>

<h1>Mereks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
