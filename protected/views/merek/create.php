<?php
$this->breadcrumbs=array(
	'Mereks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola Merek','url'=>array('admin')),
);
?>

<h1>Tambah Merek</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>