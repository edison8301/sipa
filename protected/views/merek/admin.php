<?php
$this->breadcrumbs=array(
	'Merek'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah Merek','url'=>array('create'),'icon'=>'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('merek-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Merek</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'merek-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(

		'nama',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
