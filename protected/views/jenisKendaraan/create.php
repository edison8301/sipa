<?php
$this->breadcrumbs=array(
	'Jenis Kendaraans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Kelola Jenis Kendaraan','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Tambah Jenis Kendaraan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>