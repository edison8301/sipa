<?php
$this->breadcrumbs=array(
	'Jenis Kendaraans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Tambah Jenis Kendaraan','url'=>array('create')),
array('label'=>'Update Jenis Kendaraan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Jenis Kendaraan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Kelola Jenis Kendaraan','url'=>array('admin')),
);
?>

<h1>Lihat Jenis Kendaraan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
