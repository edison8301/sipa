<?php
$this->breadcrumbs=array(
	'Jenis Kendaraan'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'Tambah Jenis Kendaraan','url'=>array('create'),'icon'=>'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('jenis-kendaraan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Jenis Kendaraan</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'jenis-kendaraan-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
