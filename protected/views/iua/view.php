<?php
$this->breadcrumbs=array(
	'IUA'=>array('admin'),
	$model->id,
);

$this->menu=array(
array('label'=>'Tambah Izin Usaha Angkutan', 'icon'=>'plus','url'=>array('create')),
array('label'=>'Edit Izin Usaha Angkutan','icon'=>'pencil', 'url'=>array('update','id'=>$model->id)),
array('label'=>'Hapus Izin Usaha Angkutan','icon'=>'trash', 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Daftar Izin Usaha Angkutan','icon'=>'list', 'url'=>array('admin')),
);
?>

<h1>Ijin Usaha Angkutan Nomor <?php echo $model->nomor; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'attributes'=>array(	
			'nomor',
			'tahun',
			'nama_perusahaan',
			'nama_pemilik',
			'alamat_pemilik',
			array(
				'label'=>'Jenis Usaha',
				'value'=>$model->jenisUsaha->nama
			),
			'tgl_ditetapkan',
		),
)); ?>

<?php
$detail_iua = new CActiveDataProvider('IuaDetail', array(
                                                            'criteria'=>array(
                                                            'condition'=>'iua_id=:ID',
                                                            'params'=>array(':ID'=>$model->id),
                                                            //'order'=>'create_time DESC',
                                                                        ),
                                                        ));
?>
<fieldset style="background: #F5F5F5; padding: 10px;">
<legend>Daftar Kendaraan</legend>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'iua-detail-grid',
		'type'=>'bordered hover striped',
		'dataProvider'=>$detail_iua,
		//'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                    'htmlOptions'=>array(
					'style'=>'text-align:center',
                                        'width'=>'20px',
					),
                ),
			'nomor_kendaraan',
			array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kendaraan_id',
				'header'=>'Jenis Kendaraan',
				'type'=>'raw',
				'value'=>'$data->jenisKendaraan->nama',
				'filter'=>CHtml::listData(JenisKendaraan::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'merek_id',
				'header'=>'Merek',
				'type'=>'raw',
				'value'=>'$data->merek->nama',
				'filter'=>CHtml::listData(JenisKendaraan::model()->findAll(),'id','nama')
			),
			'tipe',
			'tahun_pembuatan',
			'da_barang',
			'da_orang',
			'jenis_usaha',
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                 'header'=>'Options',
                    'template'=>'{update}{delete}',
                    'htmlOptions'=>array(
					     'style'=>'width: 60px',
					     'style'=>'text-align:center',
					     ),
                    'buttons' => array(
					'update' => array(
					'url'=>'Yii::app()->createUrl("iuaDetail/update", array("id"=>$data->id))', 
					'click' => "function(e){
						e.preventDefault();
						$('#update-dialog').children(':eq(0)').empty(); // Stop auto POST
						updateDialog($(this).attr('href'));
						$('#update-dialog').dialog({title:'Update Data Kendaraan'}).dialog('open');
					}",
				),
				'delete' => array(
					'url'=>'Yii::app()->createUrl("iuaDetail/delete", array("id"=>$data->id))', 
				),
			),
                    'htmlOptions'=>array(
					'style'=>'text-align:center',
					),
                ),
		array(
			'class'=>'CDataColumn',
			'name'=>'id',
			'header'=>'Print Tanda Bukti',
			'type'=>'raw',
			'value'=>'CHTml::link("<i class=\"icon-print\"></i>",array("iua/printTandaBukti","iua_detail_id"=>$data->id),array("target"=>"_blank"))',
		),
	),
)); ?>
</fieldset>

    <div class="form-actions">
            <div class="pull-right">
            <?php echo CHtml::button(Yii::t('view','Tambah Kendaraan'),array(
                    'class'=>'btn btn-success',
                    'onclick'=>'addArchive();
                    $("#update-dialog").dialog({title:"Tambah Kendaraan"}).dialog("open");
                    return false;',
                    )); ?>
            
			<?php $this->widget('bootstrap.widgets.TbButton',array('icon'=>' icon-print icon-white',
                                                                   'type'=>'warning',
                                                                   'buttonType'=>'link',
                                                                   'htmlOptions'=>array('target'=>'_blank'),
																   'label'=>'Print Izin Usaha',
                                                                   'url'=>array('printIua', 'id'=>$model->id)));?>
																   
            <?php $this->widget('bootstrap.widgets.TbButton',array('icon'=>' icon-print icon-white',
                                                                   'type'=>'warning',
                                                                   'buttonType'=>'link',
                                                                   'label'=>'Print Daftar Kendaraan',
																   'htmlOptions'=>array('target'=>'_blank'),
                                                                   'url'=>array('printDaftarKendaraan', 'iua_id'=>$model->id)));?>
                                                                   
            
			
            </div>
    </div>


<script type="text/javascript">

function addArchive()
{
    <?php 
	echo CHtml::ajax(array(
            'url'=>array('iuaDetail/create','iua_id'=>$model->id),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
			{
				if (data.status == 'failure')
				{
					$('#update-dialog div.update-dialog-content').html(data.content);
					// Here is the trick: on submit-> once again this function!
					$('#update-dialog div.update-dialog-content form').submit(addArchive);
				}
				else
				{
					$.fn.yiiGridView.update('iua-detail-grid');
					$('#update-dialog div.update-dialog-content').html(data.content);
					setTimeout(\"$('#update-dialog').dialog('close') \",1000);
					
				} 
			} ",
            ))
	?>;
    return false; 
}
</script>

<?php $this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
	'id' => 'update-dialog',
	'options' => array(
		'title' => 'Dialog',
		'autoOpen' => false,
		'modal' => true,
		'width' => 700,
		'height' => 500,
		'resizable' => true,
		'position' =>'center',
	),
)); 
echo '<div class="update-dialog-content"></div>';
$this->endWidget(); ?>

<?php
  $updateJS = CHtml::ajax( array(
  'url' => "js:url",
  'data' => "js:form.serialize() + action",
  'type' => 'post',
  'dataType' => 'json',
  'success' => "function( data )
  {
    if( data.status == 'failure' )
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      $( '#update-dialog div.update-dialog-content form input[type=submit]' )
        .die() // Stop from re-binding event handlers
        .live( 'click', function( e ){ // Send clicked button value
          e.preventDefault();
          updateDialog( false, $( this ).attr( 'name' ) );
      });
    }
    else
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      if( data.status == 'success' ) // Update all grid views on success
      {
        $( 'div.grid-view' ).each( function(){ // Change the selector if you use different class or element
          $.fn.yiiGridView.update( $( this ).attr( 'id' ) );
        });
      }
      setTimeout( \"$( '#update-dialog' ).dialog( 'close' ).children( ':eq(0)' ).empty();\", 1000 );
    }
  }"
)); ?>

<?php
Yii::app()->clientScript->registerScript( 'updateDialog', "
function updateDialog( url, act )
{
  var action = '';
  var form = $( '#update-dialog div.update-dialog-content form' );
  if( url == false )
  {
    action = '&action=' + act;
    url = form.attr( 'action' );
  }
  {$updateJS}
}" ); ?>

<?php
Yii::app()->clientScript->registerScript( 'updateDialogCreate', "
jQuery( function($){
    $( 'a.update-dialog-create' ).bind( 'click', function( e ){
      e.preventDefault();
      $( '#update-dialog' ).children( ':eq(0)' ).empty();
      updateDialog( $( this ).attr( 'href' ) );
      $( '#update-dialog' )
        .dialog( { title: 'Create' } )
        .dialog( 'open' );
    });
});
" );?>
