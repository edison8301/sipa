<?php
$this->breadcrumbs=array(
	'IUA'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'Tambah Izin Usaha Angkutan', 'icon'=>'plus','url'=>array('create')),
	array('label'=>'Lihat Izin Usaha Angkutan', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->id)),
	array('label'=>'Daftar Izin Usaha Angkutan', 'icon'=>'list', 'url'=>array('admin')),
	);
	?>

	<h1>Edit Izin Usaha Angkutan Nomor <?php echo $model->nomor; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>