<?php
$this->breadcrumbs=array(
	'IUA'=>array('admin'),
	'Create',
);

?>

<h1>Tambah Izin Usaha Angkutan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>