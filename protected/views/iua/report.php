<?php
$this->breadcrumbs=array(
	'IUA'=>array('admin'),
	'Laporan',
);

?>

<h1>Laporan Izin Usaha Angkutan</h1>


<?php
	$tanggal_awal = date('Y-m').'-01';
	$tanggal_akhir = date('Y-m').'-'.date('t');
	$tanggal = $tanggal_awal;
	
	if(isset($_POST['tanggal_awal'])) $tanggal_awal = $_POST['tanggal_awal'];
	if(isset($_POST['tanggal_akhir'])) $tanggal_akhir = $_POST['tanggal_akhir'];
?>

<?php print CHtml::beginForm(); ?>

<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'tanggal_awal',
		// additional javascript options for the date picker plugin
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>'yy-mm-dd',
			'changeYear'=>true,
			'changeMonth'=>true
		),
		'htmlOptions'=>array(
			'style'=>'height:20px;'
		),
		'value'=>$tanggal_awal
)); ?>

<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'tanggal_akhir',
		// additional javascript options for the date picker plugin
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>'yy-mm-dd',
			'changeYear'=>true,
			'changeMonth'=>true
		),
		'htmlOptions'=>array(
			'style'=>'height:20px;'
		),
		'value'=>$tanggal_akhir
)); ?>
<br>
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','type'=>'primary','icon'=>'search white','label'=>'Filter Data')); ?>

<?php print CHtml::endForm(); ?>



<table class="table">
<tr>
	<th style="text-align:center">NO</th>
	<th style="text-align:center">TANGGAL</th>
	<th style="text-align:center">IUA PENUMPANG</th>
	<th style="text-align:center">IUA BARANG</th>
	<th style="text-align:center">JUMLAH</th>
</tr>

<?php $penumpang = 0; $barang = 0; $total_penumpang = 0; $total_barang = 0; $total = 0; ?>

<?php $i=1; while($tanggal <= $tanggal_akhir) { ?>
<?php
	$penumpang = Iua::model()->countIua($tanggal,1);
	$barang = Iua::model()->countIua($tanggal,2);
	
	$total_penumpang = $total_penumpang + $penumpang;
	$total_barang = $total_barang + $barang;
?>

<tr>
	<td style="text-align:center"><?php print $i; ?></td>
	<td style="text-align:center"><?php print Bantu::tanggalSingkat($tanggal); ?></td>
	<td style="text-align:center"><?php print $penumpang; ?></td>
	<td style="text-align:center"><?php print $barang; ?></td>
	<td style="text-align:center"><?php print intval($penumpang)+intval($barang); ?></td>
</tr>

<?php $tanggal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($tanggal)).'+ 1 day')); ?>
	
<?php $i++; } ?>

<tr>
	<th style="text-align:center" colspan="2">JUMLAH</th>
	<th style="text-align:center"><?php print $total_penumpang; ?></th>
	<th style="text-align:center"><?php print $total_barang; ?></th>
	<th style="text-align:center"><?php print intval($total_penumpang)+intval($total_barang); ?></th>
</tr>	

</table>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','url'=>array('iua/reportExcel','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir),'label'=>'Export Excel','type'=>'primary','icon'=>'download-alt white')); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','url'=>array('iua/reportWord','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir),'label'=>'Export Word','type'=>'primary','icon'=>'download-alt white')); ?>

