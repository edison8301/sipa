<html>
    <head>
        <title>Print Lampiran Izin Usaha Angkutan</title>
    </head>
<body id="daf_ken"  onLoad="window.print()">
    <?php
    $no =1;
    $space = 0;
    foreach($model as $data){?>
    <div class="no_urut" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "no_urut")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "no_urut")->x;?>px;
    ">
        <?php echo $no.'.';?>
    </div>
    <div class="no_kendaraan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "no_kendaraan")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "no_kendaraan")->x;?>px;
    ">
        <?php echo $data->nomor_kendaraan;?>
    </div>
    <div class="jenis_kendaraan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "jenis_kendaraan")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "jenis_kendaraan")->x;?>px;
    ">
        <?php echo JenisKendaraan::model()->findByPk($data->jenis_kendaraan_id)->nama;?>
    </div>
    <div class="merek" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "merek")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "merek")->x;?>px;
    ">
        <?php echo Merek::model()->findByPk($data->merek_id)->nama.' '.$data->tipe;?>
    </div>
    <div class="thn_pembuatan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "thn_pembuatan")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "thn_pembuatan")->x;?>px;
    ">
        <?php echo $data->tahun_pembuatan;?>
    </div>
    <div class="da_barang" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "da_barang")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "da_barang")->x;?>px;
    ">
        <?php echo $data->da_barang;?>
    </div>
    <div class="da_orang" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "da_orang")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "da_orang")->x;?>px;
    ">
        <?php echo $data->da_orang;?>
    </div>
    <div class="jenis_usaha"style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "jenis_usaha")->y+$space;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "jenis_usaha")->x;?>px;
    ">
        <?php echo $data->jenis_usaha;?>
    </div>
    
    <?php
    $no++;
    $space = $space+30;
    }?>
    
    <div class="tanggal_dokumen" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("daf_ken", "tanggal_dokumen")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("daf_ken", "tanggal_dokumen")->x;?>px;
    ">
        <?php print Tools::getMonth(Iua::model()->findByPk($data->iua_id)->tgl_ditetapkan);?>
    </div>
</body>
</html>
