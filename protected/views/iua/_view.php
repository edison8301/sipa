<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_perusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_perusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_usaha_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_usaha_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_ditetapkan')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_ditetapkan); ?>
	<br />

	*/ ?>

</div>