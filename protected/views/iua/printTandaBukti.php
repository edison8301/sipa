<html>
    <head>
        <title>Print Tanda Bukti Pemegang Izin Usaha Angkutan</title>
    </head>
<body id="iua" onLoad="window.print()">

    
    <div class="nama_perusahaan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_perusahaan")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_perusahaan")->x;?>px;
    ">
        <?php echo $model->nama_perusahaan;?>
    </div>
    
    <div class="nama" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_pemilik")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_pemilik")->x;?>px;
    ">
        <?php echo $model->nama_pemilik;?>
    </div>
    
    <div class="alamat" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "alamat")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "alamat")->x;?>px;
    ">
        <?php echo $model->alamat_pemilik;?>
    </div>
    
    <div class="jenis_usaha" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "jenis_usaha")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "jenis_usaha")->x;?>px;
    ">
        <?php echo JenisUsaha::model()->findByPk($model->jenis_usaha_id)->nama;?>
    </div>
	
	<div class="nomor_kendaraan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "nomor_kendaraan")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "nomor_kendaraan")->x;?>px;
    ">
        <?php echo $iuaDetail->nomor_kendaraan;?>
    </div>
	
	<div class="nama_perusahaan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_perusahaan_arsip")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_perusahaan_arsip")->x;?>px;
    ">
        <?php echo $model->nama_perusahaan;?>
    </div>
    
    <div class="nama" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_pemilik_arsip")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "nama_pemilik_arsip")->x;?>px;
    ">
        <?php echo $model->nama_pemilik;?>
    </div>
    
    <div class="alamat" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "alamat_arsip")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "alamat_arsip")->x;?>px;
    ">
        <?php echo $model->alamat_pemilik;?>
    </div>
    
    <div class="jenis_usaha" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "jenis_usaha_arsip")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "jenis_usaha_arsip")->x;?>px;
    ">
        <?php echo JenisUsaha::model()->findByPk($model->jenis_usaha_id)->nama;?>
    </div>
	
	<div class="nomor_kendaraan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "nomor_kendaraan_arsip")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "nomor_kendaraan_arsip")->x;?>px;
    ">
        <?php echo $iuaDetail->nomor_kendaraan;?>
    </div>
     
     <div class="tanggal_dokumen" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("tanda_bukti", "tanggal_ditetapkan")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("tanda_bukti", "tanggal_ditetapkan")->x;?>px;
    ">
        <?php echo Tools::getMonth($model->tgl_ditetapkan);?>
    </div>
</body>
</html>
