<html>
    <head>
        <title>Print Izin Usaha Angkutan</title>
    </head>
<body id="iua" onLoad="window.print()">
    <div class="nomor" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("iua", "nomor")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("iua", "nomor")->x;?>px;
    ">
        <?php echo $model->nomor.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$model->tahun;?>
    </div>
    
    <div class="nama_perusahaan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("iua", "nama_perusahaan")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("iua", "nama_perusahaan")->x;?>px;
    ">
        <?php echo $model->nama_perusahaan;?>
    </div>
    
    <div class="nama" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("iua", "nama")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("iua", "nama")->x;?>px;
    ">
        <?php echo $model->nama_pemilik;?>
    </div>
    
    <div class="alamat" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("iua", "alamat")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("iua", "alamat")->x;?>px;
    ">
        <?php echo $model->alamat_pemilik;?>
    </div>
    
    <div class="jenis_usaha" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("iua", "jenis_usaha")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("iua", "jenis_usaha")->x;?>px;
    ">
        <?php echo JenisUsaha::model()->findByPk($model->jenis_usaha_id)->nama;?>
    </div>
     
     <div class="tanggal_dokumen" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("iua", "tanggal_dokumen")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("iua", "tanggal_dokumen")->x;?>px;
    ">
        <?php echo Tools::getMonth($model->tgl_ditetapkan);?>
    </div>
</body>
</html>
