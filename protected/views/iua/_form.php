<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'iua-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tahun',array('class'=>'span2','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'nama_perusahaan',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nama_pemilik',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'alamat_pemilik',array('rows'=>4, 'cols'=>50, 'class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'jenis_usaha_id',CHtml::listData(JenisUsaha::model()->findAll(), 'id', 'nama'), array('prompt'=>'-- Pilih Jenis Usaha --')); ?>

	
	<div class="control-group ">
		<label class="control-label required" for="Produk_kategori_produk_id">
			Tanggal Berkas
		</label>
		<div class="controls">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'Iua[tgl_ditetapkan]',
				'id'=>'Iua_tgl_ditetapkan',
				'language' => 'id',
				'model' => $model,
				'value'=>$model->tgl_ditetapkan,
				// additional javascript options for the date picker plugin
				'options'=>array(
				    'showAnim'=>'fold',
				    'showOn'=>'button',
				    'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.png',
				    'dateFormat'=>'yy-mm-dd',
				    'changeMonth' => 'false',
				    'showButtonPanel' => 'false',
				    'changeYear'=>'false',
				    'constrainInput' => 'false',
				),
				'htmlOptions'=>array(
				    'style'=>'height:20px;width:150px; margin-bottom:0px',
				    'readonly'=>true,
				),
			));?>
		</div>
	</div>
	
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
