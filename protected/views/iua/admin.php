<?php
$this->breadcrumbs=array(
	'IUA'=>array('admin'),
	'Kelola',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('iua-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Daftar Izin Usaha Angkutan</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'iua-grid',
'type'=>'bordered hover striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
		    'header'=>'No',
		    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                    'htmlOptions'=>array(
					'style'=>'text-align:center',
                                        'width'=>'20px',
					),
                ),
		'nomor',
		'tahun',
		'nama_perusahaan',
		'nama_pemilik',
		'alamat_pemilik',
		/*
		'jenis_usaha_id',
		'tgl_ditetapkan',
		*/
array(
	'header'=>'Pilihan',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
