<?php
$this->breadcrumbs=array(
	'Ibms',
);

$this->menu=array(
array('label'=>'Create Ibm','url'=>array('create')),
array('label'=>'Manage Ibm','url'=>array('admin')),
);
?>

<h1>Ibms</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
