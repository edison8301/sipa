<?php
$this->breadcrumbs=array(
	'IBM'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Tambah Izin Bongkar','url'=>array('create'),'icon'=>'plus'),
	array('label'=>'Lihat Izin Bongkar','url'=>array('view','id'=>$model->id),'icon'=>'eye-open'),
	array('label'=>'Kelola Izin Bongkar','url'=>array('admin'),'icon'=>'list'),
);

?>

	<h1>Update Izin Bongkar Muat</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>