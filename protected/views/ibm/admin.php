<?php
$this->breadcrumbs=array(
	'Ibms'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('ibm-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Izin Bongkar Muat</h1>

<?php /*
<?php echo CHtml::link('Pencarian Lanjut','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/ ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'ibm-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nomor',
			'nama',
			'alamat',
			'nomor_kendaraan',
			array(
				'class'=>'CDataColumn',
				'name'=>'jenis_kendaraan_id',
				'header'=>'Jenis Kendaraan',
				'value'=>'$data->jenisKendaraan->nama',
				'filter'=>CHtml::listData(JenisKendaraan::model()->findAll(),'id','nama')
			),
			'tanggal',
			array(
				'class'=>'CDataColumn',
				'name'=>'id',
				'header'=>'Print',
				'type'=>'raw',
				'value'=>'CHtml::link("<i class=\'icon-print\' ></i>",array("ibm/print","id"=>$data->id),array("target"=>"_blank"))',
				'filter'=>''
			),
			array(
				'header'=>'Pilihan',
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
