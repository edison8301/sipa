<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_kendaraan')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_kendaraan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kendaraan_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kendaraan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_berlaku_awal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_berlaku_awal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_berlaku_akhir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_berlaku_akhir); ?>
	<br />

	*/ ?>

</div>