<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'ibm-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>4, 'cols'=>50, 'class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nomor_kendaraan',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'jenis_kendaraan_id',CHtml::listData(JenisKendaraan::model()->findAll(),'id','nama')); ?>
	
	<div class="control-group ">
		<label class="control-label required" for="Produk_kategori_produk_id">
			Tanggal Berkas
		</label>
		<div class="controls">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'Ibm[tanggal]',
				'id'=>'Ibm_tanggal',
				'language' => 'id',
				'model' => $model,
				'value'=>$model->tanggal,
				// additional javascript options for the date picker plugin
				'options'=>array(
				    'showAnim'=>'fold',
				    'showOn'=>'button',
				    'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.png',
				    'dateFormat'=>'yy-mm-dd',
				    'changeMonth' => 'false',
				    'showButtonPanel' => 'false',
				    'changeYear'=>'false',
				    'constrainInput' => 'false',
				),
				'htmlOptions'=>array(
				    'style'=>'height:20px;width:150px; margin-bottom:0px',
				    'readonly'=>true,
				),
			));?>
		</div>
	</div>
	
	<div class="control-group ">
		<label class="control-label required" for="Produk_kategori_produk_id">
			Tanggal Berlaku Awal
		</label>
		<div class="controls">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'Ibm[tanggal_berlaku_awal]',
				'id'=>'Ibm_tanggal_berlaku_awal',
				'language' => 'id',
				'model' => $model,
				'value'=>$model->tanggal_berlaku_awal,
				// additional javascript options for the date picker plugin
				'options'=>array(
				    'showAnim'=>'fold',
				    'showOn'=>'button',
				    'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.png',
				    'dateFormat'=>'yy-mm-dd',
				    'changeMonth' => 'false',
				    'showButtonPanel' => 'false',
				    'changeYear'=>'false',
				    'constrainInput' => 'false',
				),
				'htmlOptions'=>array(
				    'style'=>'height:20px;width:150px; margin-bottom:0px',
				    'readonly'=>true,
				),
			));?>
		</div>
	</div>
	
	<div class="control-group ">
		<label class="control-label required" for="Produk_kategori_produk_id">
			Tanggal Berlaku Akhir 
		</label>
		<div class="controls">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'Ibm[tanggal_berlaku_akhir]',
				'id'=>'Ibm_tanggal_berlaku_akhir',
				'language' => 'id',
				'model' => $model,
				'value'=>$model->tanggal_berlaku_akhir,
				// additional javascript options for the date picker plugin
				'options'=>array(
				    'showAnim'=>'fold',
				    'showOn'=>'button',
				    'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.png',
				    'dateFormat'=>'yy-mm-dd',
				    'changeMonth' => 'false',
				    'showButtonPanel' => 'false',
				    'changeYear'=>'false',
				    'constrainInput' => 'false',
				),
				'htmlOptions'=>array(
				    'style'=>'height:20px;width:150px; margin-bottom:0px',
				    'readonly'=>true,
				),
			));?>
		</div>
	</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>$model->isNewRecord ? 'Create' : 'Save')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'type'=>'primary', 'icon'=>'repeat white', 'label'=>'Reset')); ?>
</div>

<?php $this->endWidget(); ?>
