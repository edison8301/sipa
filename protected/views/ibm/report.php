<?php
$this->breadcrumbs=array(
	'IUA'=>array('admin'),
	'Laporan',
);

?>

<h1>Laporan Izin Bongkar Muat</h1>


<?php
	$tanggal_awal = date('Y-m').'-01';
	$tanggal_akhir = date('Y-m').'-'.date('t');
	$tanggal = $tanggal_awal;
	
	if(isset($_POST['tanggal_awal'])) $tanggal_awal = $_POST['tanggal_awal'];
	if(isset($_POST['tanggal_akhir'])) $tanggal_akhir = $_POST['tanggal_akhir'];
?>

<?php print CHtml::beginForm(); ?>

<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'tanggal_awal',
		// additional javascript options for the date picker plugin
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>'yy-mm-dd',
			'changeYear'=>true,
			'changeMonth'=>true
		),
		'htmlOptions'=>array(
			'style'=>'height:20px;'
		),
		'value'=>$tanggal_awal
)); ?>

<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
		'name'=>'tanggal_akhir',
		// additional javascript options for the date picker plugin
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>'yy-mm-dd',
			'changeYear'=>true,
			'changeMonth'=>true
		),
		'htmlOptions'=>array(
			'style'=>'height:20px;'
		),
		'value'=>$tanggal_akhir
)); ?>
<br>
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','type'=>'primary','icon'=>'search white','label'=>'Filter Data')); ?>

<?php print CHtml::endForm(); ?>



<table class="table">
<tr>
	<th style="text-align:center">NO</th>
	<th style="text-align:center">TANGGAL</th>
	<?php foreach(JenisKendaraan::model()->findAll() as $data) { ?>
	<th style="text-align:center"><?php print $data->nama; ?></th>
	<?php } ?>
	<th style="text-align:center">JUMLAH</th>
</tr>

<?php $penumpang = 0; $barang = 0; $total_penumpang = 0; $total_barang = 0; $total = 0; ?>
<?php
	
	$total = array();
	foreach(JenisKendaraan::model()->findAll() as $data)
	{
		$total[$data->id] = 0;
	}
?>
<?php $i=1; while($tanggal <= $tanggal_akhir) { ?>
<?php
	
	$jumlah = array();
	$subtotal = 0;
	foreach(JenisKendaraan::model()->findAll() as $data) {
		$jumlah[$data->id] = Ibm::model()->countIbm($tanggal,$data->id);
		$subtotal = $subtotal + $jumlah[$data->id];
		$total[$data->id] = $total[$data->id] + $jumlah[$data->id];
	}
	
?>

<tr>
	<td style="text-align:center"><?php print $i; ?></td>
	<td style="text-align:center"><?php print Bantu::tanggalSingkat($tanggal); ?></td>
	<?php foreach(JenisKendaraan::model()->findAll() as $data) { ?>
	<td style="text-align:center"><?php print $jumlah[$data->id]; ?></td>
	<?php } ?>
	<td style="text-align:center"><?php print $subtotal; ?></td>
</tr>

<?php $tanggal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($tanggal)).'+ 1 day')); ?>
	
<?php $i++; } ?>

<tr>
	<th style="text-align:center">&nbsp;</th>
	<th style="text-align:center">JUMLAH</th>
	<?php $grandTotal = 0; ?>
	<?php foreach(JenisKendaraan::model()->findAll() as $data) { ?>
	<?php $grandTotal = $grandTotal + $total[$data->id]; ?>
	<th style="text-align:center"><?php print $total[$data->id]; ?></th>
	<?php } ?>
	<th style="text-align:center"><?php print $grandTotal; ?></th>
</tr>

</table>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','url'=>array('ibm/reportExcel','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir),'label'=>'Export Excel','type'=>'primary','icon'=>'download-alt white')); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','url'=>array('ibm/reportWord','tanggal_awal'=>$tanggal_awal,'tanggal_akhir'=>$tanggal_akhir),'label'=>'Export Word','type'=>'primary','icon'=>'download-alt white')); ?>

