<html>
    <head>
        <title>Print Ijin Bongkar Muat</title>
    </head>
<body id="ibm" onLoad="window.print()">
    <div class="nomor" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "nomor")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "nomor")->x;?>px;
    ">
        <?php echo $model->nomor;?>
    </div>
    
    <div class="nama" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "nama")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "nama")->x;?>px;
    ">
        <?php echo $model->nama;?>
    </div>
    
    <div class="alamat" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "alamat")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "alamat")->x;?>px;
    ">
        <?php echo $model->alamat;?>
    </div>
    
    <div class="no_kendaraan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "no_kendaraan")->y; ?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "no_kendaraan")->x; ?>px;
    ">
        <?php echo $model->nomor_kendaraan;?>
    </div>
    
    <div class="jenis_kendaraan" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "jenis_kendaraan")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "jenis_kendaraan")->x;?>px;
    ">
        <?php echo JenisKendaraan::model()->findByPk($model->jenis_kendaraan_id)->nama;?>
    </div>
    
     <div class="tanggal_berlaku" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "tanggal_berlaku")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "tanggal_berlaku")->x;?>px;
    ">
        <?php echo Tools::getMonth($model->tanggal_berlaku_awal).'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.Tools::getMonth($model->tanggal_berlaku_akhir);?>
    </div>
     
     <div class="tanggal_dokumen" style="position: absolute;
        margin-top:<?php echo PrintSetting::model()->getValue("ibm", "tanggal_dokumen")->y;?>px;
        margin-left: <?php echo PrintSetting::model()->getValue("ibm", "tanggal_dokumen")->x;?>px;
    ">
        <?php echo Tools::getMonth($model->tanggal);?>
    </div>
</body>
</html>
