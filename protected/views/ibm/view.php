<?php
$this->breadcrumbs=array(
	'Ibms'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Tambah Izin Bongkar','url'=>array('create'),'icon'=>'plus'),
array('label'=>'Update Izin Bongkar','url'=>array('update','id'=>$model->id),'icon'=>'pencil'),
array('label'=>'Delete Izin Bongkar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'trash'),
array('label'=>'Kelola Izin Bongkar','url'=>array('admin'),'icon'=>'th-list'),
);
?>

<h1>Izin Bongkar Muat <?php echo $model->nomor;?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'nomor',
		'nama',
		'alamat',
		'nomor_kendaraan',
		array(
			'label'=>'Jenis Kendaraan',
			'value'=>$model->jenisKendaraan->nama
		),
		'tanggal',
		'tanggal_berlaku_awal',
		'tanggal_berlaku_akhir',
),
)); ?>

<div class="form-actions">
    <div class="pull-right">
        <?php $this->widget('bootstrap.widgets.TbButton',array('icon'=>' icon-print icon-white',
                                                                       'type'=>'warning',
                                                                       'buttonType'=>'link',
                                                                       'htmlOptions'=>array('target'=>'_blank'),
																	   'label'=>'Print Izin Bongkar Muat',
                                                                       'url'=>array('print', 'id'=>$model->id)));?>
    </div>
</div>
