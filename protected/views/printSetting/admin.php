<?php
$this->breadcrumbs=array(
	'Print Settings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Tambah Posisi Print','url'=>array('create'),'icon'=>'plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('print-setting-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Pengaturan Posisi Print</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'print-setting-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'dokumen',
			'keterangan',
			'elemen',
			'x',
			'y',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
