<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'print-setting-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'dokumen',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'elemen',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'x',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'y',array('class'=>'span5')); ?>
	
	<?php echo $form->textFieldRow($model,'keterangan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
