<?php
$this->breadcrumbs=array(
	'Print Settings'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List PrintSetting','url'=>array('index')),
array('label'=>'Manage PrintSetting','url'=>array('admin')),
);
?>

<h1>Create PrintSetting</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>