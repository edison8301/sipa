<?php
$this->breadcrumbs=array(
	'Print Settings',
);

$this->menu=array(
array('label'=>'Create PrintSetting','url'=>array('create')),
array('label'=>'Manage PrintSetting','url'=>array('admin')),
);
?>

<h1>Print Settings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
