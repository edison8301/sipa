<?php
$this->breadcrumbs=array(
	'Print Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PrintSetting','url'=>array('index')),
array('label'=>'Create PrintSetting','url'=>array('create')),
array('label'=>'Update PrintSetting','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete PrintSetting','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PrintSetting','url'=>array('admin')),
);
?>

<h1>View PrintSetting #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'dokumen',
		'elemen',
		'x',
		'y',
                'keterangan',
),
)); ?>
