<?php
$this->breadcrumbs=array(
	'Print Settings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List PrintSetting','url'=>array('index')),
	array('label'=>'Create PrintSetting','url'=>array('create')),
	array('label'=>'View PrintSetting','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PrintSetting','url'=>array('admin')),
	);
	?>

	<h1>Update PrintSetting <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>