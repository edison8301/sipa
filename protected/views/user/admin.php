<?php
$this->breadcrumbs=array(
	'User'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	array('label'=>'User','url'=>array('admin'),'icon'=>'list'),
	array('label'=>'Tambah', 'icon'=>'plus', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola User</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
			),
		),
		'username',
		array(
			'class'=>'CDataColumn',
			'name'=>'role_id',
			'header'=>'Role',
			'type'=>'raw',
			'value'=>'$data->role->name',
			'filter'=>CHtml::listData(Role::model()->findAll(),'id','name')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'status',
			'header'=>'Status',
			'type'=>'raw',
			'value'=>'$data->status == 1 ? "Aktif" : "Tidak Aktif"',
			'filter'=>array('0'=>'Tidak Aktif','1'=>'Aktif')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'id',
			'header'=>'Ganti Password',
			'type'=>'raw',
			'value'=>'CHtml::link("<i class=\"icon-lock\"></i>",array("user/updatePassword","id"=>$data->id))',
			'filter'=>''
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}'
		),
	),
)); ?>
