<?php
$this->breadcrumbs=array(
	'Kendaraan'=>array('admin'),
	'Kelola',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('iua-detail-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Daftar Kendaraan</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'iua-detail-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'class'=>'CDataColumn',
			'name'=>'id',
			'header'=>'No IUA',
			'type'=>'raw',
			'value'=>'CHTml::link($data->iua->nomor,array("iua/view","id"=>$data->iua_id))',
			'filter'=>''
		),
		'nomor_kendaraan',
		array(
			'class'=>'CDataColumn',
			'name'=>'jenis_kendaraan_id',
			'header'=>'Jenis Kendaraan',
			'type'=>'raw',
			'value'=>'$data->jenisKendaraan->nama',
			'filter'=>CHtml::listData(JenisKendaraan::model()->findAll(),'id','nama')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'merek_id',
			'header'=>'Merek',
			'type'=>'raw',
			'value'=>'$data->merek->nama',
			'filter'=>CHtml::listData(Merek::model()->findAll(),'id','nama')
		),
		'tipe',
		/*
		'tahun_pembuatan',
		'da_barang',
		'da_orang',
		'jenis_usaha',
		*/

	),
)); ?>
