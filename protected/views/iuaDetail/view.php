<?php
$this->breadcrumbs=array(
	'Iua Details'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List IuaDetail','url'=>array('index')),
array('label'=>'Create IuaDetail','url'=>array('create')),
array('label'=>'Update IuaDetail','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete IuaDetail','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage IuaDetail','url'=>array('admin')),
);
?>

<h1>View IuaDetail #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'iua_id',
		'nomor_kendaraan',
		'jenis_kendaraan_id',
		'merek_id',
		'tipe',
		'tahun_pembuatan',
		'da_barang',
		'da_orang',
		'jenis_usaha',
),
)); ?>
