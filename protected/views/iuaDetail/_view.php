<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iua_id')); ?>:</b>
	<?php echo CHtml::encode($data->iua_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_kendaraan')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_kendaraan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kendaraan_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kendaraan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('merek_id')); ?>:</b>
	<?php echo CHtml::encode($data->merek_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe')); ?>:</b>
	<?php echo CHtml::encode($data->tipe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun_pembuatan')); ?>:</b>
	<?php echo CHtml::encode($data->tahun_pembuatan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('da_barang')); ?>:</b>
	<?php echo CHtml::encode($data->da_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('da_orang')); ?>:</b>
	<?php echo CHtml::encode($data->da_orang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_usaha')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_usaha); ?>
	<br />

	*/ ?>

</div>