<?php
$this->breadcrumbs=array(
	'Iua Details'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List IuaDetail','url'=>array('index')),
array('label'=>'Manage IuaDetail','url'=>array('admin')),
);
?>

<h1>Create IuaDetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>