<?php
$this->breadcrumbs=array(
	'Iua Details',
);

$this->menu=array(
array('label'=>'Create IuaDetail','url'=>array('create')),
array('label'=>'Manage IuaDetail','url'=>array('admin')),
);
?>

<h1>Iua Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
