<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'iua-detail-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'iua_id',array('class'=>'span5', 'value'=>$iua_id)); ?>

	<?php echo $form->textFieldRow($model,'nomor_kendaraan',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'jenis_kendaraan_id', CHtml::listData(JenisKendaraan::model()->findAll(), 'id', 'nama'), array('prompt'=>'-- Pilih Jenis Kendaraan --')); ?>
	
	<?php echo $form->dropDownListRow($model,'merek_id',CHtml::listData(Merek::model()->findAll(), 'id', 'nama'), array('prompt'=>'-- Pilih Merek --')); ?>

	<?php echo $form->textFieldRow($model,'tipe',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'tahun_pembuatan',array('class'=>'span1','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'da_barang',array('class'=>'span2')); ?>

	<?php echo $form->textFieldRow($model,'da_orang',array('class'=>'span2')); ?>

	<?php echo $form->textFieldRow($model,'jenis_usaha',array('class'=>'span3','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>$model->isNewRecord ? 'Create' : 'Save')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'type'=>'primary', 'icon'=>'repeat white', 'label'=>'Reset')); ?>
</div>

<?php $this->endWidget(); ?>
