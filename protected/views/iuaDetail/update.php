<?php
$this->breadcrumbs=array(
	'Iua Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List IuaDetail','url'=>array('index')),
	array('label'=>'Create IuaDetail','url'=>array('create')),
	array('label'=>'View IuaDetail','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage IuaDetail','url'=>array('admin')),
	);
	?>

	<h1>Update IuaDetail <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>