<?php

class IuaController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'printIua', 'printDaftarKendaraan','printTandaBukti',
							'report','reportExcel','reportWord'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public $menu = array(
		array('label'=>'Daftar IUA', 'icon'=>'list','url'=>array('iua/admin')),
		array('label'=>'Daftar Kendaraan', 'icon'=>'list','url'=>array('iuaDetail/admin')),
		array('label'=>'Tambah IUA', 'icon'=>'plus','url'=>array('create')),
		array('label'=>'Laporan IUA', 'icon'=>'book','url'=>array('report')),
	);
/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Iua;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Iua']))
{
$model->attributes=$_POST['Iua'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Iua']))
{
$model->attributes=$_POST['Iua'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Iua');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Iua('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Iua']))
$model->attributes=$_GET['Iua'];

$this->render('admin',array(
'model'=>$model,
));
}

	public function actionReport()
	{
		$this->render('report');
	}
	
	public function actionReportWord()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPWord',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$PHPWord = new PHPWord();
		
		$PHPWord->addFontStyle('bold', array('bold'=>true));
		$PHPWord->addFontStyle('subjudul', array('name'=>'Arial','bold'=>true, 'size'=>11));
		$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));
		
		$tableStyle = array(
					'borderSize'=>1, 
					'borderColor'=>'000000', 
					'cellMargin'=>80,
					'border'=>true
		);
			
		$PHPWord->addTableStyle('tableStyle', $tableStyle);
		
		$center = array('align'=>'center');
		
		$paraStyle = array('spaceAfter'=>'2','align'=>'center');
		$headStyle = array('spaceAfter'=>'2','align'=>'center');
		
		$section = $PHPWord->createSection(array('marginLeft'=>1000, 'marginRight'=>1000, 'marginTop'=>1000, 'marginBottom'=>1000));
		
		$section->addText('LAPORAN PENGELUARAN IZIN USAHA ANGKUTAN',array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
		$tanggal_awal = $_GET['tanggal_awal'];
		$tanggal_akhir = $_GET['tanggal_akhir'];
		
		$tanggal = $tanggal_awal;
		
		
		$section->addText(Bantu::tanggalSingkat($tanggal_awal).' S.D '.Bantu::tanggalSingkat($tanggal_akhir),array('bold'=>true,'name'=>'Times New Roman','size'=>'14'),$center);		
		
		
		$section->addTextBreak(1);		
		
		$table = $section->addTable('tableStyle');
		
		$table->addRow();
			
		$table->addCell(500)->addText("NO",array('bold'=>'true'),$headStyle);
			
		$table->addCell(2000)->addText('TANGGAL',array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText('IUA PENUMPANG',array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText('IUA BARANG',array('bold'=>'true'),$headStyle);
		$table->addCell(2000)->addText('JUMLAH',array('bold'=>'true'),$headStyle);
		
		$i=1;
		
		
		

		$penumpang = 0; $barang = 0; $total_penumpang = 0; $total_barang = 0; $total = 0; 


		$i=1; 
		
		while($tanggal <= $tanggal_akhir) {
			
			$penumpang = Iua::model()->countIua($tanggal,1);
			$barang = Iua::model()->countIua($tanggal,2);
	
			$total_penumpang = $total_penumpang + $penumpang;
			$total_barang = $total_barang + $barang;
			$penumpang_barang = $penumpang + $barang;
			$total_penumpang_barang = $total_penumpang + $total_barang;
			
			$table->addRow();
			$table->addCell(100)->addText($i,array(),$paraStyle);
			$table->addCell(100)->addText(Bantu::tanggalSingkat($tanggal),array(),$paraStyle);
			$table->addCell(100)->addText($penumpang,array(),$paraStyle);
			$table->addCell(100)->addText($barang,array(),$paraStyle);
			$table->addCell(100)->addText($penumpang_barang,array(),$paraStyle);
	
			$i++;

			$tanggal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($tanggal)).'+ 1 day'));
		}
		
		$table->addRow();
		$table->addCell(100)->addText("",array(),$paraStyle);
		$table->addCell(100)->addText("JUMLAH",array('bold'=>true),$paraStyle);
		$table->addCell(100)->addText($total_penumpang,array('bold'=>true),$paraStyle);
		$table->addCell(100)->addText($total_barang,array('bold'=>true),$paraStyle);
		$table->addCell(100)->addText($total_penumpang_barang,array('bold'=>true),$paraStyle);
		
	
		$section->addTextBreak(1);		
		
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		
		$pathFile = Yii::app()->basePath.'/../exports/';
		
		$filename = time().'_LaporanIUA.docx';
		
		$objWriter->save($pathFile.$filename);
		
		$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);
	
	}
	
	public function actionReportExcel()
	{
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.vendors.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));

		$PHPExcel = new PHPExcel();
		
		$tanggal_awal = $_GET['tanggal_awal'];
		$tanggal_akhir = $_GET['tanggal_akhir'];
		
		$tanggal = $tanggal_awal;
		
		$PHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN IZIN USAHA ANGKUTAN');
		$PHPExcel->getActiveSheet()->setCellValue('A2', Bantu::tanggalSingkat($tanggal_awal).' S.D '.Bantu::tanggalSingkat($tanggal_akhir));
		
		$PHPExcel->getActiveSheet()->setCellValue('A4', 'NO');
		$PHPExcel->getActiveSheet()->setCellValue('B4', 'TANGGAL');
		$PHPExcel->getActiveSheet()->setCellValue('C4', 'IUA PENUMPANG');
		$PHPExcel->getActiveSheet()->setCellValue('D4', 'IUA BARANG');
		$PHPExcel->getActiveSheet()->setCellValue('E4', 'JUMLAH');
						
		$PHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
			
		$PHPExcel->getActiveSheet()->getStyle('A4:E4')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);				
		$PHPExcel->getActiveSheet()->getStyle('A4:E4')->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle('A4:E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);	
		$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);		
		$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		
			
			
		$i = 1;
		$kolom = 5;

		while($tanggal <= $tanggal_akhir) {
			
			$penumpang = Iua::model()->countIua($tanggal,1);
			$barang = Iua::model()->countIua($tanggal,2);
	
			$total_penumpang = $total_penumpang + $penumpang;
			$total_barang = $total_barang + $barang;
			$penumpang_barang = $penumpang + $barang;
			$total_penumpang_barang = $total_penumpang + $total_barang;
			
			$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
			$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, Bantu::tanggalSingkat($tanggal));
			$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $penumpang);
			$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $barang);
			$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $penumpang_barang);
									
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
			
			$i++; $kolom++;
			
			$tanggal = date('Y-m-d',strtotime(date('Y-m-d',strtotime($tanggal)).'+ 1 day'));
		}
		
		$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, "");
		$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, "JUMLAH");
		$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $total_penumpang);
		$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $total_barang);
		$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $total_penumpang_barang);
									
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$PHPExcel->getActiveSheet()->getStyle('A'.$kolom.':E'.$kolom)->getFont()->setBold(true);
		
		
		$filename = time().'_LaporanIUA.xlsx';

		$path = Yii::app()->basePath.'/../exports/';
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save($path.$filename);	
		$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);
	
	}

public function actionPrintIua($id)
{
    $this->layout = '//layouts/plain';
    $model = $this->loadModel($id);
    
    $this->render('printIua', array('model'=>$model));
}

public function actionPrintDaftarKendaraan($iua_id)
{
    $this->layout = '//layouts/plain';
    $model = IuaDetail::model()->findAllByAttributes(array('iua_id'=>$iua_id));
    
    $this->render('printDaftarKendaraan', array('model'=>$model));
}

public function actionPrintTandaBukti()
{
    $this->layout = '//layouts/plain';
	
	$iuaDetail = IuaDetail::model()->findByPk($_GET['iua_detail_id']);
    $model = $this->loadModel($iuaDetail->iua_id);
    
    $this->render('printTandaBukti', array('model'=>$model,'iuaDetail'=>$iuaDetail));
}


/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Iua::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='iua-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
