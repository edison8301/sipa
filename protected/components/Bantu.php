<?php

class Bantu {

	public function rp($jumlah)
	{
		$output = "Rp ".number_format($jumlah,0,',','.');
		
		return $output;
	}
	
	public function tanggalSingkat($tanggal=null)
	{
		$output = '';
		
		if($tanggal!=null)
		{
			$tanggal = explode('-',$tanggal);
				
			$output = $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
		}
		
		return $output;
	}

}

?>