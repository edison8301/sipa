<?php

/**
 * This is the model class for table "ibm".
 *
 * The followings are the available columns in table 'ibm':
 * @property integer $id
 * @property string $nomor
 * @property string $nama
 * @property string $alamat
 * @property string $nomor_kendaraan
 * @property integer $jenis_kendaraan_id
 * @property string $tanggal
 * @property string $tanggal_berlaku_awal
 * @property string $tanggal_berlaku_akhir
 */
class Ibm extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ibm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_kendaraan_id', 'numerical', 'integerOnly'=>true),
			array('nomor, nama, nomor_kendaraan', 'length', 'max'=>255),
			array('alamat, tanggal, tanggal_berlaku_awal, tanggal_berlaku_akhir', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nomor, nama, alamat, nomor_kendaraan, jenis_kendaraan_id, tanggal, tanggal_berlaku_awal, tanggal_berlaku_akhir', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jenisKendaraan'=>array(self::BELONGS_TO,'JenisKendaraan','jenis_kendaraan_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor Berkas',
			'nama' => 'Nama',
			'alamat' => 'Alamat',
			'nomor_kendaraan' => 'Nomor Kendaraan',
			'jenis_kendaraan_id' => 'Jenis Kendaraan',
			'tanggal' => 'Tanggal Berkas',
			'tanggal_berlaku_awal' => 'Tanggal Berlaku Awal',
			'tanggal_berlaku_akhir' => 'Tanggal Berlaku Akhir',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('nomor_kendaraan',$this->nomor_kendaraan,true);
		$criteria->compare('jenis_kendaraan_id',$this->jenis_kendaraan_id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('tanggal_berlaku_awal',$this->tanggal_berlaku_awal,true);
		$criteria->compare('tanggal_berlaku_akhir',$this->tanggal_berlaku_akhir,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ibm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function countIbm($tanggal,$jenis_kendaraan_id)
	{
		return Ibm::model()->countByAttributes(array('tanggal'=>$tanggal,'jenis_kendaraan_id'=>$jenis_kendaraan_id));
	}	
}
