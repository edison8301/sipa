<?php

/**
 * This is the model class for table "iua_detail".
 *
 * The followings are the available columns in table 'iua_detail':
 * @property integer $id
 * @property integer $iua_id
 * @property string $nomor_kendaraan
 * @property integer $jenis_kendaraan_id
 * @property integer $merek_id
 * @property string $tipe
 * @property string $tahun_pembuatan
 * @property integer $da_barang
 * @property integer $da_orang
 * @property string $jenis_usaha
 *
 * The followings are the available model relations:
 * @property Iua $iua
 */
class IuaDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'iua_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('iua_id', 'required'),
			array('iua_id, jenis_kendaraan_id, merek_id, da_barang, da_orang', 'numerical', 'integerOnly'=>true),
			array('nomor_kendaraan, tipe, jenis_usaha', 'length', 'max'=>255),
			array('tahun_pembuatan', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, iua_id, nomor_kendaraan, jenis_kendaraan_id, merek_id, tipe, tahun_pembuatan, da_barang, da_orang, jenis_usaha', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iua' => array(self::BELONGS_TO, 'Iua', 'iua_id'),
			'jenisKendaraan'=>array(self::BELONGS_TO,'JenisKendaraan','jenis_kendaraan_id'),
			'merek'=>array(self::BELONGS_TO,'Merek','merek_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'iua_id' => 'Iua',
			'nomor_kendaraan' => 'Nomor Kendaraan',
			'jenis_kendaraan_id' => 'Jenis Kendaraan',
			'merek_id' => 'Merek',
			'tipe' => 'Tipe',
			'tahun_pembuatan' => 'Tahun Pembuatan',
			'da_barang' => 'Daya Angkut Barang',
			'da_orang' => 'Daya Angkut Orang',
			'jenis_usaha' => 'Jenis Usaha',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('iua_id',$this->iua_id);
		$criteria->compare('nomor_kendaraan',$this->nomor_kendaraan,true);
		$criteria->compare('jenis_kendaraan_id',$this->jenis_kendaraan_id);
		$criteria->compare('merek_id',$this->merek_id);
		$criteria->compare('tipe',$this->tipe,true);
		$criteria->compare('tahun_pembuatan',$this->tahun_pembuatan,true);
		$criteria->compare('da_barang',$this->da_barang);
		$criteria->compare('da_orang',$this->da_orang);
		$criteria->compare('jenis_usaha',$this->jenis_usaha,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IuaDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
