<?php

/**
 * This is the model class for table "iua".
 *
 * The followings are the available columns in table 'iua':
 * @property integer $id
 * @property string $nomor
 * @property string $tahun
 * @property string $nama_perusahaan
 * @property string $nama_pemilik
 * @property string $alamat_pemilik
 * @property integer $jenis_usaha_id
 * @property string $tgl_ditetapkan
 *
 * The followings are the available model relations:
 * @property IuaDetail[] $iuaDetails
 */
class Iua extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'iua';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_pemilik, tgl_ditetapkan', 'required'),
			array('jenis_usaha_id', 'numerical', 'integerOnly'=>true),
			array('nomor, nama_perusahaan, nama_pemilik', 'length', 'max'=>255),
			array('tahun', 'length', 'max'=>4),
			array('alamat_pemilik', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nomor, tahun, nama_perusahaan, nama_pemilik, alamat_pemilik, jenis_usaha_id, tgl_ditetapkan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iuaDetails' => array(self::HAS_MANY, 'IuaDetail', 'iua_id'),
			'jenisUsaha'=>array(self::BELONGS_TO,'JenisUsaha','jenis_usaha_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor Berkas',
			'tahun' => 'Tahun',
			'nama_perusahaan' => 'Nama Perusahaan',
			'nama_pemilik' => 'Nama Pemilik',
			'alamat_pemilik' => 'Alamat Pemilik',
			'jenis_usaha_id' => 'Jenis Usaha',
			'tgl_ditetapkan' => 'Tgl Berkas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('nama_perusahaan',$this->nama_perusahaan,true);
		$criteria->compare('nama_pemilik',$this->nama_pemilik,true);
		$criteria->compare('alamat_pemilik',$this->alamat_pemilik,true);
		$criteria->compare('jenis_usaha_id',$this->jenis_usaha_id);
		$criteria->compare('tgl_ditetapkan',$this->tgl_ditetapkan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Iua the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function countIua($tanggal,$jenis_usaha_id)
	{
		return Iua::model()->countByAttributes(array('tgl_ditetapkan'=>$tanggal,'jenis_usaha_id'=>$jenis_usaha_id));
	}	
}
