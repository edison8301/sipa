<div class="navbar navbar-inverse">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="#">SIPA Angkutan</a>
          
          <div class="nav-collapse">
			<?php $this->widget('bootstrap.widgets.TbMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
                        array('label'=>'Dashboard', 'url'=>array('/site/index')),
                        array('label'=>'Izin Usaha Angkutan', 'url'=>'#','items'=>array(
							array('label'=>'Daftar IUA','url'=>array('/iua/admin'),'icon'=>'list'),
							array('label'=>'Daftar Kendaraan','url'=>array('/iuaDetail/admin'),'icon'=>'list'),
							array('label'=>'Tambah IUA','url'=>array('/iua/create'),'icon'=>'plus'),
							array('label'=>'Laporan IUA','url'=>array('/iua/report'),'icon'=>'book'),
						)),
						array('label'=>'Izin Bongkar Muat', 'url'=>array('/ibm/admin'),'items'=>array(
							array('label'=>'Daftar Izin Bongkar Muat','url'=>array('/ibm/admin'),'icon'=>'list'),
							array('label'=>'Tambah IBM','url'=>array('/ibm/create'),'icon'=>'plus'),
							array('label'=>'Laporan IBM','url'=>array('/ibm/report'),'icon'=>'book'),
						)),
                        array('label'=>'Master Data', 'url'=>'#','items'=>array(
							array('label'=>'Jenis Kendaraan', 'url'=>array('/jenisKendaraan/admin'),'icon'=>'tags'),
							array('label'=>'Merek', 'url'=>array('/merek/admin'),'icon'=>'tags'),
							array('label'=>'Jenis Usaha', 'url'=>array('/jenisUsaha/admin'),'icon'=>'tags'),
                        )),
                        array('label'=>'Setting', 'url'=>'#','items'=>array(
							array('label'=>'Print Setting', 'url'=>array('/printSetting/admin'),'icon'=>'tags'),
							array('label'=>'Backup', 'url'=>array('/jbackup/default'),'icon'=>'tags'),
							array('label'=>'User', 'url'=>array('/user/admin'),'icon'=>'tags'),
							//array('label'=>'Role', 'url'=>array('/role/admin'),'icon'=>'tags'),	
                        )),
                        array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

<?php /*
<div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        
        	<div class="style-switcher pull-left">
                <a href="javascript:chooseStyle('none', 60)" checked="checked"><span class="style" style="background-color:#0088CC;"></span></a>
                <a href="javascript:chooseStyle('style2', 60)"><span class="style" style="background-color:#7c5706;"></span></a>
                <a href="javascript:chooseStyle('style3', 60)"><span class="style" style="background-color:#468847;"></span></a>
                <a href="javascript:chooseStyle('style4', 60)"><span class="style" style="background-color:#4e4e4e;"></span></a>
                <a href="javascript:chooseStyle('style5', 60)"><span class="style" style="background-color:#d85515;"></span></a>
                <a href="javascript:chooseStyle('style6', 60)"><span class="style" style="background-color:#a00a69;"></span></a>
                <a href="javascript:chooseStyle('style7', 60)"><span class="style" style="background-color:#a30c22;"></span></a>
          	</div>
           <form class="navbar-search pull-right" action="">
           	 
           <input type="text" class="search-query span2" placeholder="Search">
           
           </form>
    	</div><!-- container -->
    </div><!-- navbar-inner -->
</div><!-- subnav -->
*/ ?>